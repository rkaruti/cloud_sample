-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 16, 2022 at 07:30 PM
-- Server version: 10.1.25-MariaDB
-- PHP Version: 7.1.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `identitybased`
--

-- --------------------------------------------------------

--
-- Table structure for table `attackerdetails`
--

CREATE TABLE `attackerdetails` (
  `id` int(10) NOT NULL,
  `filename` varchar(25) DEFAULT NULL,
  `hashkey` varchar(50) DEFAULT NULL,
  `server` varchar(30) DEFAULT NULL,
  `content` text,
  `attackerid` varchar(25) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `attackerdetails`
--

INSERT INTO `attackerdetails` (`id`, `filename`, `hashkey`, `server`, `content`, `attackerid`) VALUES
(5, 'test.txt', '85549348', 'CS2', 'Hello Javaasdf', 'attacker');

-- --------------------------------------------------------

--
-- Table structure for table `dataowner`
--

CREATE TABLE `dataowner` (
  `uid` int(10) NOT NULL,
  `fname` varchar(50) DEFAULT NULL,
  `lname` varchar(50) DEFAULT NULL,
  `uname` varchar(50) DEFAULT NULL,
  `upass` varchar(50) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `phone` varchar(15) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `dataowner`
--

INSERT INTO `dataowner` (`uid`, `fname`, `lname`, `uname`, `upass`, `email`, `phone`) VALUES
(5, 'Bharath', 'K', 'bharath', 'tiger', 'bharath@gmail.com', NULL),
(6, 'Owner', 'Owner', 'owner', 'Tiger', 'ramesh.karuti@gmail.com', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `filedetails`
--

CREATE TABLE `filedetails` (
  `id` int(10) NOT NULL,
  `filename` varchar(25) DEFAULT NULL,
  `filekey` varchar(50) DEFAULT NULL,
  `hashkey` varchar(50) DEFAULT NULL,
  `userid` int(10) DEFAULT NULL,
  `server` varchar(30) DEFAULT NULL,
  `content` text,
  `auth` int(10) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `filedetails`
--

INSERT INTO `filedetails` (`id`, `filename`, `filekey`, `hashkey`, `userid`, `server`, `content`, `auth`) VALUES
(7, 'c.txt', '940838996', '506141002', 2, 'CS1', 'V2VsY29tZSB0byBNZWdhaW5mb3dhcmUu', 2),
(21, 'test_1.txt', '1412148233', '431969124', 4, 'CS1', 'V2VsY29tZSB0byBNZWdhaW5mb3dhcmU=', 1),
(22, 'test_1.txt', '1412148233', '431969124', 5, 'CS1', 'V2VsY29tZSB0byBNZWdhaW5mb3dhcmU=', 1),
(23, 'test_2.txt', '-1991954621', '2048348613', 5, 'CS1', 'SGkgV2VsY29tZSB0byBtZWdhaW5mb3dhcmVIaSBXZWxjb21lIHRvIG1lZ2FpbmZvd2FyZUhpIFdlbGNvbWUgdG8gbWVnYWluZm93YXJlSGkgV2VsY29tZSB0byBtZWdhaW5mb3dhcmVIaSBXZWxjb21lIHRvIG1lZ2FpbmZvd2FyZUhpIFdlbGNvbWUgdG8gbWVnYWluZm93YXJlSGkgV2VsY29tZSB0byBtZWdhaW5mb3dhcmVIaSBXZWxjb21lIHRvIG1lZ2FpbmZvd2FyZUhpIFdlbGNvbWUgdG8gbWVnYWluZm93YXJlSGkgV3Zmc2Z2ZWVsY29tZSB0byBtZWdhaW5mb3dhcmVIaSBXZWxjb21lIHRvIG1lZ2FpbmZvd2FyZUhpIFdlbGNvbWUgYXNkZnRvIG1lZ2FpbmZvd2FyZUhpIFdlbGNvbWUgdG8gbWVnYWluZm93YXJlSGkgV2VsY29tZSB0byBtZWdhaW5mb3dhcmVIaSBXZWxjb21lIHRvIG1lZ2FpbmZvd2FyZUhpIFdlbGNvbWUgdG8gbWVnYWluZm93YXJlSGkgV2VsY29tZSB0byBtZWdhaW5mb3dhcmVIaSBXZWxjb21lIHRvIG1lZ2FpbmZvd2FyZUhpc2FmYSBXZWxjb21lIHRvIG1lZ2FpbmZvd2FyZUhpIFdlbGNvbWUgdG8gbWVnYWluZm93YXJlSGkgV2VsY29tZSB0byBtZWdhaW5mb3dhcmVIaSBXZWxjb21lIHRvIGZXZWxjb21lIHRvIG1lZ2FpbmZvd2FyZUhpIFdlbGNvbWUgdG8gbWVnYWluZm93YXJlSGkgV2VsY29tZSB0byBtZWdhaW5mb3dhcmVIaSBXZWxjb21lIHRvIG1lZ2FpbmZvd2FyZUhpIFdlbGNvbWUgdG8gbWVnYWluZm93YXJlSGkgV2VsY29tZSB0byBtZWdhaW5mb3dhcmVIaSBXZWxjb21lIHRvIG1lZ2FpbmZvd2FyZUhpIFdlbGNvbWUgdG8gbWVnYWluZm93YXJlSGkgV2VsY29tZSB0byBtZWdhaW5mb3dhcmVIaSBXZWxjb21lIHRvIG1lZ2FpbmZvd2FyZUhpIFdlbGNvbWUgdG8gbWVnYWluZm93YXJlSGkgV2VsY29tZSB0byBtZWdhaW5mb3dhcmVIaSBXZWxjb21lIHRvIG1lZ2FpbmZvd2FyZUhpIFdlbGNvbWUgdG8gbWVnYWluZm93YXJlSGkgV2VsY29tZSB0byBtZWdhaW5mb3dhcmVIaSBXZWxjb21lIHRvIG1lZ2FpbmZvd2FyZUhpIFdlbGNvbWUgdG8gbWVnYWluZm93YXJlSGkgV2VsY29tZSB0byBtZWdhaW5mb3dhcmVIaSBXZWxjb21lIHRvIG1lZ2FpbmZvd2FyZUhpIFdlbGNvbWUgdG8gbWVnYWluZm93YXJlSGkgV2VsY29tZSB0byBtZWdhaW5mb3dhcmVIaSBXZWxjb21lIHRvIG1lZ2FpbmZvd2FyZUhpIFdlbGNvbWUgdG8gbWVnYWluZm93YXJlSGkgV2VsY29tZSB0byBtZWdhaW5mb3dhcmVIaSBXZWxjb21lIHRvIG1lZ2FpbmZvd2FyZUhpIFdlbGNvbWUgdG8gbWVnYWluZm93YXJlSGkgV2VsY29tZSB0byBtZWdhaW5mb3dhcmUsSGkgV2VsY29tZSB0byBtZWdhaW5mb3dhcmVIaSBXZWxjb21lIHRvIG1lZ2FpbmZvd2FyZUhpIFdlbGNvbWUgdG8gbWVnYWluZm93YXJlSGkgV2VsY29tZSB0byBtZWdhaW5mb3dhcmVIaSBXZWxjb21lIHRvIG1lZ2FpbmZvd2FyZUhpIFdlbGNvbWUgdG8gbWVnYWluZm93YXJlSGkgV2VsY29tZSB0byBtZWdhaW5mb3dhcmVIaSBXZWxjb21lIHRvIG1lZ2FpbmZvd2FyZUhpIFdlbGNvbWUgdG8gbWVnYWluZm93YXJlSGkgV2VsY29tZSB0byBtZWdhaW5mb3dhcmVIaSBXZWxjb21lIHRvIG1lZ2FpbmZvd2FyZUhpIFdlbGNvbWUgdG8gbWVnYWluZm93YXJlSGkgV2VsY29tZSB0byBtZWdhaW5mb3dhcmVIaSBXZWxjb21lIHRvIG1lZ2FpbmZvd2FyZUhpIFdlbGNvbWUgdG8gbWVnYWluZm93YXJlSGkgV2VsY29tZSB0byBtZWdhaW5mb3dhcmVIaSBXZWxjb21lIHRvIG1lZ2FpbmZvd2FyZUhpIFdlbGNvbWUgdG8gbWVnYWluZm93YXJlSGkgV2VsY29tZSB0byBtZWdhaW5mb3dhcmVIaSBXZWxjb21lIHRvIG1lZ2FpbmZvd2FyZUhpIFdlbGNvbWUgdG8gbWVnYWluZm93YXJlSGkgV2VsY29tZSB0byBtZWdhaW5mb3dhcmVIaSBXZWxjb21lIHRvIG1lZ2FpbmZvd2FyZUhpIFdlbGNvbWUgdG8gbWVnYWluZm93YXJlSGkgV2VsY29tZSB0byBtZWdhaW5mb3dhcmVIaSBXZWxjb21lIHRvIG1lZ2FpbmZvd2FyZUhpIFdlbGNvbWUgdG8gbWVnYWluZm93YXJlSGkgV2VsY29tZSB0byBtZWdhaW5mb3dhcmVIaSBXZWxjb21lIHRvIG1lZ2FpbmZvd2FyZUhpIFdlbGNvbWUgdG8gbWVnYWluZm93YXJlSGkgV2VsY29tZSB0byBtZWdhaW5mb3dhcmVIaSBXZWxjb21lIHRvIG1lZ2FpbmZvd2FyZUhpIFdlbGNvbWUgdG8gbWVnYWluZm93YXJlSGkgV2VsY29tZSB0byBtZWdhaW5mb3dhcmVIaSBXZWxjb21lIHRvIG1lZ2FpbmZvd2FyZUhpIFdlbGNvbWUgdG8gbWVnYWluZm93YXJlSGkgV2VsY29tZSB0byBtZWdhaW5mb3dhcmVIaSBXZWxjb21lIHRvIG1lZ2FpbmZvd2FyZUhpIFdlbGNvbWUgdG8gbWVnYWluZm93YXJlSGkgV2VsY29tZSB0byBtZWdhaW5mb3dhcmVIaSBXZWxjb21lIHRvIG1lZ2FpbmZvd2FyZUhpIFdlbGNvbWUgdG8gbWVnYWluZm93YXJlSGkgV2VsY29tZSB0byBtZWdhaW5mb3dhcmVIaSBXZWxjb21lIHRvIG1lZ2FpbmZvd2FyZUhpIFdlbGNvbWUgdG8gbWVnYWluZm93YXJlSGkgV2VsY29tZSB0byBtZWdhaW5mb3dhcmVIaSBXZWxjb21lIHRvIG1lZ2FpbmZvd2FyZUhpIFdlbGNvbWUgdG8gbWVnYWluZm93YXJlSGkgV2VsY29tZSB0byBtZWdhaW5mb3dhcmUsSGkgV2VsY29tZSB0byBtZWdhaW5mb3dhcmVIaSBXZWxjb21lIHRvIG1lZ2FpbmZvd2FyZUhpIFdlbGNvbWUgdG8gbWVnYWluZm93YXJlSGkgV2VsY29tZSB0byBtZWdhaW5mb3dhcmVIaSBXZWxjb21lIHRvIG1lZ2FpbmZvd2FyZUhpIFdlbGNvbWUgdG8gbWVnYWluZm93YXJlSGkgV2VsY29tZSB0byBtZWdhaW5mb3dhcmVIaSBXZWxjb21lIHRvIG1lZ2FpbmZvd2FyZUhpIFdlbGNvbWUgdG8gbWVnYWluZm93YXJlSGkgV2VsY29tZSB0byBtZWdhaW5mb3dhcmVIaSBXZWxjb21lIHRvIG1lZ2FpbmZvd2FyZUhpIFdlbGNvbWUgdG8gbWVnYWluZm93YXJlSGkgV2VsY29tZSB0byBtZWdhaW5mb3dhcmVIaSBXZWxjb21lIHRvIG1lZ2FpbmZvd2FyZUhpIFdlbGNvbWUgdG8gbWVnYWluZm93YXJlSGkgV2VsY29tZSB0byBtZWdhaW5mb3dhcmVIaSBXZWxjb21lIHRvIG1lZ2FpbmZvd2FyZUhpIFdlbGNvbWUgdG8gbWVnYWluZm93YXJlSGkgV2VsY29tZSB0byBtZWdhaW5mb3dhcmVIaSBXZWxjb21lIHRvIG1lZ2FpbmZvd2FyZUhpIFdlbGNvbWUgdG8gbWVnYWluZm93YXJlSGkgV2VsY29tZSB0byBtZWdhaW5mb3dhcmVIaSBXZWxjb21lIHRvIG1lZ2FpbmZvd2FyZUhpIFdlbGNvbWUgdG8gbWVnYWluZm93YXJlSGkgV2VsY29tZSB0byBtZWdhaW5mb3dhcmVIaSBXZWxjb21lIHRvIG1lZ2FpbmZvd2FyZUhpIFdlbGNvbWUgdG8gbWVnYWluZm93YXJlSGkgV2VsY29tZSB0byBtZWdhaW5mb3dhcmVIaSBXZWxjb21lIHRvIG1lZ2FpbmZvd2FyZUhpIFdlbGNvbWUgdG8gbWVnYWluZm93YXJlSGkgV2VsY29tZSB0byBtZWdhaW5mb3dhcmVIaSBXZWxjb21lIHRvIG1lZ2FpbmZvd2FyZUhpIFdlbGNvbWUgdG8gbWVnYWluZm93YXJlSGkgV2VsY29tZSB0byBtZWdhaW5mb3dhcmVIaSBXZWxjb21lIHRvIG1lZ2FpbmZvd2FyZUhpIFdlbGNvbWUgdG8gbWVnYWluZm93YXJlSGkgV2VsY29tZSB0byBtZWdhaW5mb3dhcmVIaSBXZWxjb21lIHRvIG1lZ2FpbmZvd2FyZUhpIFdlbGNvbWUgdG8gbWVnYWluZm93YXJlSGkgV2VsY29tZSB0byBtZWdhaW5mb3dhcmVIaSBXZWxjb21lIHRvIG1lZ2FpbmZvd2FyZUhpIFdlbGNvbWUgdG8gbWVnYWluZm93YXJlSGkgV2VsY29tZSB0byBtZWdhaW5mb3dhcmVIaSBXZWxjb21lIHRvIG1lZ2FpbmZvd2FyZUhpIFdlbGNvbWUgdG8gbWVnYWluZm93YXJlSGkgV2VsY29tZSB0byBtZWdhaW5mb3dhcmVIaSBXZWxjb21lIHRvIG1lZ2FpbmZvd2FyZUhpIFdlbGNvbWUgdG8gbWVnYWluZm93YXJlSGkgV2VsY29tZSB0byBtZWdhaW5mb3dhcmUsSGkgV2VsY29tZSB0byBtZWdhaW5mb3dhcmVIaSBXZWxjb21lIHRvIG1lZ2FpbmZvd2FyZUhpIFdlbGNvbWUgdG8gbWVnYWluZm93YXJlSGkgV2VsY29tZSB0byBtZWdhaW5mb3dhcmVIaSBXZWxjb21lIHRvIG1lZ2FpbmZvd2FyZUhpIFdlbGNvbWUgdG8gbWVnYWluZm93YXJlSGkgV2VsY29tZSB0byBtZWdhaW5mb3dhcmVIaSBXZWxjb21lIHRvIG1lZ2FpbmZvd2FyZUhpIFdlbGNvbWUgdG8gbWVnYWluZm93YXJlSGkgV2VsY29tZSB0byBtZWdhaW5mb3dhcmVIaSBXZWxjb21lIHRvIG1lZ2FpbmZvd2FyZUhpIFdlbGNvbWUgdG8gbWVnYWluZm93YXJlSGkgV2VsY29tZSB0byBtZWdhaW5mb3dhcmVIaSBXZWxjb21lIHRvIG1lZ2FpbmZvd2FyZUhpIFdlbGNvbWUgdG8gbWVnYWluZm93YXJlSGkgV2VsY29tZSB0byBtZWdhaW5mb3dhcmVIaSBXZWxjb21lIHRvIG1lZ2FpbmZvd2FyZUhpIFdlbGNvbWUgdG8gbWVnYWluZm93YXJlSGkgV2VsY29tZSB0byBtZWdhaW5mb3dhcmVIaSBXZWxjb21lIHRvIG1lZ2FpbmZvd2FyZUhpIFdlbGNvbWUgdG8gbWVnYWluZm93YXJlSGkgV2VsY29tZSB0byBtZWdhaW5mb3dhcmVIaSBXZWxjb21lIHRvIG1lZ2FpbmZvd2FyZUhpIFdlbGNvbWUgdG8gbWVnYWluZm93YXJlSGkgV2VsY29tZSB0byBtZWdhaW5mb3dhcmVIaSBXZWxjb21lIHRvIG1lZ2FpbmZvd2FyZUhpIFdlbGNvbWUgdG8gbWVnYWluZm93YXJlSGkgV2VsY29tZSB0byBtZWdhaW5mb3dhcmVIaSBXZWxjb21lIHRvIG1lZ2FpbmZvd2FyZUhpIFdlbGNvbWUgdG8gbWVnYWluZm93YXJlSGkgV2VsY29tZSB0byBtZWdhaW5mb3dhcmVIaSBXZWxjb21lIHRvIG1lZ2FpbmZvd2FyZUhpIFdlbGNvbWUgdG8gbWVnYWluZm93YXJlSGkgV2VsY29tZSB0byBtZWdhaW5mb3dhcmVIaSBXZWxjb21lIHRvIG1lZ2FpbmZvd2FyZUhpIFdlbGNvbWUgdG8gbWVnYWluZm93YXJlSGkgV2VsY29tZSB0byBtZWdhaW5mb3dhcmVIaSBXZWxjb21lIHRvIG1lZ2FpbmZvd2FyZUhpIFdlbGNvbWUgdG8gbWVnYWluZm93YXJlSGkgV2VsY29tZSB0byBtZWdhaW5mb3dhcmVIaSBXZWxjb21lIHRvIG1lZ2FpbmZvd2FyZUhpIFdlbGNvbWUgdG8gbWVnYWluZm93YXJlSGkgV2VsY29tZSB0byBtZWdhaW5mb3dhcmVIaSBXZWxjb21lIHRvIG1lZ2FpbmZvd2FyZUhpIFdlbGNvbWUgdG8gbWVnYWluZm93YXJlSGkgV2VsY29tZSB0byBtZWdhaW5mb3dhcmVIaSBXZWxjb21lIHRvIG1lZ2FpbmZvd2FyZUhpIFdlbGNvbWUgdG8gbWVnYWluZm93YXJlSGkgV2VsY29tZSB0byBtZWdhaW5mb3dhcmUsSGkgV2VsY29tZSB0byBtZWdhaW5mb3dhcmVIaSBXZWxjb21lIHRvIG1lZ2FpbmZvd2FyZUhpIFdlbGNvbWUgdG8gbWVnYWluZm93YXJlSGkgV2VsY29tZSB0byBtZWdhaW5mb3dhcmVIaSBXZWxjb21lIHRvIG1lZ2FpbmZvd2FyZUhpIFdlbGNvbWUgdG8gbWVnYWluZm93YXJlSGkgV2VsY29tZSB0byBtZWdhaW5mb3dhcmVIaSBXZWxjb21lIHRvIG1lZ2FpbmZvd2FyZUhpIFdlbGNvbWUgdG8gbWVnYWluZm93YXJlSGkgV2VsY29tZSB0byBtZWdhaW5mb3dhcmVIaSBXZWxjb21lIHRvIG1lZ2FpbmZvd2FyZUhpIFdlbGNvbWUgdG8gbWVnYWluZm93YXJlSGkgV2VsY29tZSB0byBtZWdhaW5mb3dhcmVIaSBXZWxjb21lIHRvIG1lZ2FpbmZvd2FyZUhpIFdlbGNvbWUgdG8gbWVnYWluZm93YXJlSGkgV2VsY29tZSB0byBtZWdhaW5mb3dhcmVIaSBXZWxjb21lIHRvIG1lZ2FpbmZvd2FyZUhpIFdlbGNvbWUgdG8gbWVnYWluZm93YXJlSGkgV2VsY29tZSB0byBtZWdhaW5mb3dhcmVIaSBXZWxjb21lIHRvIG1lZ2FpbmZvd2FyZUhpIFdlbGNvbWUgdG8gbWVnYWluZm93YXJlSGkgV2VsY29tZSB0byBtZWdhaW5mb3dhcmVIaSBXZWxjb21lIHRvIG1lZ2FpbmZvd2FyZUhpIFdlbGNvbWUgdG8gbWVnYWluZm93YXJlSGkgV2VsY29tZSB0byBtZWdhaW5mb3dhcmVIaSBXZWxjb21lIHRvIG1lZ2FpbmZvd2FyZUhpIFdlbGNvbWUgdG8gbWVnYWluZm93YXJlSGkgV2VsY29tZSB0byBtZWdhaW5mb3dhcmVIaSBXZWxjb21lIHRvIG1lZ2FpbmZvd2FyZUhpIFdlbGNvbWUgdG8gbWVnYWluZm93YXJlSGkgV2VsY29tZSB0byBtZWdhaW5mb3dhcmVIaSBXZWxjb21lIHRvIG1lZ2FpbmZvd2FyZUhpIFdlbGNvbWUgdG8gbWVnYWluZm93YXJlSGkgV2VsY29tZSB0byBtZWdhaW5mb3dhcmVIaSBXZWxjb21lIHRvIG1lZ2FpbmZvd2FyZUhpIFdlbGNvbWUgdG8gbWVnYWluZm93YXJlSGkgV2VsY29tZSB0byBtZWdhaW5mb3dhcmVIaSBXZWxjb21lIHRvIG1lZ2FpbmZvd2FyZUhpIFdlbGNvbWUgdG8gbWVnYWluZm93YXJlSGkgV2VsY29tZSB0byBtZWdhaW5mb3dhcmVIaSBXZWxjb21lIHRvIG1lZ2FpbmZvd2FyZUhpIFdlbGNvbWUgdG8gbWVnYWluZm93YXJlSGkgV2VsY29tZSB0byBtZWdhaW5mb3dhcmVIaSBXZWxjb21lIHRvIG1lZ2FpbmZvd2FyZUhpIFdlbGNvbWUgdG8gbWVnYWluZm93YXJlSGkgV2VsY29tZSB0byBtZWdhaW5mb3dhcmVIaSBXZWxjb21lIHRvIG1lZ2FpbmZvd2FyZUhpIFdlbGNvbWUgdG8gbWVnYWluZm93YXJlSGkgV2VsY29tZSB0byBtZWdhaW5mb3dhcmUsSGkgV2VsY29tZSB0byBtZWdhaW5mb3dhcmVIaSBXZWxjb21lIHRvIG1lZ2FpbmZvd2FyZUhpIFdlbGNvbWUgdG8gbWVnYWluZm93YXJlSGkgV2VsY29tZSB0byBtZWdhaW5mb3dhcmVIaSBXZWxjb21lIHRvIG1lZ2FpbmZvd2FyZUhpIFdlbGNvbWUgdG8gbWVnYWluZm93YXJlSGkgV2VsY29tZSB0byBtZWdhaW5mb3dhcmVIaSBXZWxjb21lIHRvIG1lZ2FpbmZvd2FyZUhpIFdlbGNvbWUgdG8gbWVnYWluZm93YXJlSGkgV2VsY29tZSB0byBtZWdhaW5mb3dhcmVIaSBXZWxjb21lIHRvIG1lZ2FpbmZvd2FyZUhpIFdlbGNvbWUgdG8gbWVnYWluZm93YXJlSGkgV2VsY29tZSB0byBtZWdhaW5mb3dhcmVIaSBXZWxjb21lIHRvIG1lZ2FpbmZvd2FyZUhpIFdlbGNvbWUgdG8gbWVnYWluZm93YXJlSGkgV2VsY29tZSB0byBtZWdhaW5mb3dhcmVIaSBXZWxjb21lIHRvIG1lZ2FpbmZvd2FyZUhpIFdlbGNvbWUgdG8gbWVnYWluZm93YXJlSGkgV2VsY29tZSB0byBtZWdhaW5mb3dhcmVIaSBXZWxjb21lIHRvIG1lZ2FpbmZvd2FyZUhpIFdlbGNvbWUgdG8gbWVnYWluZm93YXJlSGkgV2VsY29tZSB0byBtZWdhaW5mb3dhcmVIaSBXZWxjb21lIHRvIG1lZ2FpbmZvd2FyZUhpIFdlbGNvbWUgdG8gbWVnYWluZm93YXJlSGkgV2VsY29tZSB0byBtZWdhaW5mb3dhcmVIaSBXZWxjb21lIHRvIG1lZ2FpbmZvd2FyZUhpIFdlbGNvbWUgdG8gbWVnYWluZm93YXJlSGkgV2VsY29tZSB0byBtZWdhaW5mb3dhcmVIaSBXZWxjb21lIHRvIG1lZ2FpbmZvd2FyZUhpIFdlbGNvbWUgdG8gbWVnYWluZm93YXJlSGkgV2VsY29tZSB0byBtZWdhaW5mb3dhcmVIaSBXZWxjb21lIHRvIG1lZ2FpbmZvd2FyZUhpIFdlbGNvbWUgdG8gbWVnYWluZm93YXJlSGkgV2VsY29tZSB0byBtZWdhaW5mb3dhcmVIaSBXZWxjb21lIHRvIG1lZ2FpbmZvd2FyZUhpIFdlbGNvbWUgdG8gbWVnYWluZm93YXJlSGkgV2VsY29tZSB0byBtZWdhaW5mb3dhcmVIaSBXZWxjb21lIHRvIG1lZ2FpbmZvd2FyZUhpIFdlbGNvbWUgdG8gbWVnYWluZm93YXJlSGkgV2VsY29tZSB0byBtZWdhaW5mb3dhcmVIaSBXZWxjb21lIHRvIG1lZ2FpbmZvd2FyZUhpIFdlbGNvbWUgdG8gbWVnYWluZm93YXJlSGkgV2VsY29tZSB0byBtZWdhaW5mb3dhcmVIaSBXZWxjb21lIHRvIG1lZ2FpbmZvd2FyZUhpIFdlbGNvbWUgdG8gbWVnYWluZm93YXJlSGkgV2VsY29tZSB0byBtZWdhaW5mb3dhcmVIaSBXZWxjb21lIHRvIG1lZ2FpbmZvd2FyZUhpIFdlbGNvbWUgdG8gbWVnYWluZm93YXJlSGk=', 1),
(24, 'example1.txt', '1572237443', '-295868628', 6, 'CS1', 'c2VsZWN0IHBpZCwgdWlkLCBiaWRfcHJpY2UgZnJvbSBiaWRkaW5nIHdoZXJlIHBpZCA9ICc3OGlvJw0KDQoNClNFTEVDVCBwaWQsIHVpZCwgTUFYKGJpZF9wcmljZSApIEFTICdNYXggQmlkJw0KRlJPTSBiaWRkaW5nIA0KV0hFUkUgcGlkID0gJzc4aW8nDQpPUkRFUiBCWSAnTWF4IEJpZCcgQVNDOw0KDQpTRUxFQ1QgcGlkLCB1aWQsIE1BWChiaWRfcHJpY2UgKSBBUyAnTWF4IEJpZCcgRlJPTSBiaWRkaW5nIFdIRVJFIHBpZCA9ICc3OGlvJyBPUkRFUiBCWSAnTWF4IEJpZCcgQVNDDQoNCnNlbGVjdCB0YmwucGlkLCB0YmwudWlkLCB0YmwuYmlkX3ByaWNlIGZyb20gYmlkZGluZyB0Ymwgam9pbiAoc2VsZWN0IG1heChiaWRfcHJpY2UpIGFzIE1heEJpZCBmcm9tIGJpZGRpbmcpIHRibDEgb24gdGJsMS5NYXhCaWQgPSB0YmwuYmlkX3ByaWNlIEdST1VQIEJZIFBJRCBPUkRFUiBCWSBkYXRlX3RpbWUgQVNDDQoNClNFTEVDVCBwaWQsIHVpZCwgTUFYKGJpZF9wcmljZSkgQVMgJ01heCBCaWQnIEZST00gYmlkZGluZyBHUk9VUCBCWSBwaWQgT1JERVIgQlkgTUFYKGJpZF9wcmljZSkgQVNDDQoNClNFTEVDVCBwaWQsIHVpZCwNCk1BWChiaWRfcHJpY2UpIA0KRlJPTSBiaWRkaW5nIA0KR1JPVVAgQlkgcGlkDQphc2Rm', 1);

-- --------------------------------------------------------

--
-- Table structure for table `proxy`
--

CREATE TABLE `proxy` (
  `uid` int(11) NOT NULL,
  `uname` varchar(100) NOT NULL,
  `upass` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `authstatus` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `proxy`
--

INSERT INTO `proxy` (`uid`, `uname`, `upass`, `email`, `authstatus`) VALUES
(1, 'p', '123456', 'p@g.com', 1);

-- --------------------------------------------------------

--
-- Table structure for table `proxyfiles`
--

CREATE TABLE `proxyfiles` (
  `uid` int(11) NOT NULL,
  `fname` varchar(100) NOT NULL,
  `uname` varchar(100) NOT NULL,
  `filename` varchar(100) NOT NULL,
  `auth` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `proxyfiles`
--

INSERT INTO `proxyfiles` (`uid`, `fname`, `uname`, `filename`, `auth`) VALUES
(1, 'owner', 'p', 'asd.txt', 0);

-- --------------------------------------------------------

--
-- Table structure for table `userdetails`
--

CREATE TABLE `userdetails` (
  `uid` int(10) NOT NULL,
  `fname` varchar(250) DEFAULT '0',
  `lname` varchar(250) DEFAULT '0',
  `uname` varchar(250) DEFAULT '0',
  `pass` varchar(250) DEFAULT '0',
  `email` varchar(250) DEFAULT '0',
  `dob` varchar(250) DEFAULT '0',
  `gender` varchar(250) DEFAULT '0',
  `phone` varchar(250) DEFAULT '0',
  `nationality` varchar(250) DEFAULT '0',
  `userstatus` varchar(25) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `userdetails`
--

INSERT INTO `userdetails` (`uid`, `fname`, `lname`, `uname`, `pass`, `email`, `dob`, `gender`, `phone`, `nationality`, `userstatus`) VALUES
(5, 'Rose', 'Marry', 'rose', 'tiger', 'ramesh.karuti@yahoo.co.in', '2004-06-09', 'Female', '9898787676', 'Indian', '1');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `attackerdetails`
--
ALTER TABLE `attackerdetails`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `dataowner`
--
ALTER TABLE `dataowner`
  ADD PRIMARY KEY (`uid`);

--
-- Indexes for table `filedetails`
--
ALTER TABLE `filedetails`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `proxy`
--
ALTER TABLE `proxy`
  ADD PRIMARY KEY (`uid`);

--
-- Indexes for table `proxyfiles`
--
ALTER TABLE `proxyfiles`
  ADD PRIMARY KEY (`uid`);

--
-- Indexes for table `userdetails`
--
ALTER TABLE `userdetails`
  ADD PRIMARY KEY (`uid`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `attackerdetails`
--
ALTER TABLE `attackerdetails`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `dataowner`
--
ALTER TABLE `dataowner`
  MODIFY `uid` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `filedetails`
--
ALTER TABLE `filedetails`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;
--
-- AUTO_INCREMENT for table `proxy`
--
ALTER TABLE `proxy`
  MODIFY `uid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `proxyfiles`
--
ALTER TABLE `proxyfiles`
  MODIFY `uid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `userdetails`
--
ALTER TABLE `userdetails`
  MODIFY `uid` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
